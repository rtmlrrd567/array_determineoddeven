﻿/*
 * Created by SharpDevelop.
 * User: Evanice
 * Date: 9/6/2022
 * Time: 7:27 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace Activity_1
{
	class Program
	{
		public static void Main(string[] args)
		{
			int [] arr = new int[5];
			int i;
			for(i = 0; i<arr.Length; i++)
			{
				Console.Write("Enter Elements [{0}]:", i);
				arr[i] = Convert.ToInt32(Console.ReadLine());
			}
			Console.WriteLine("Odd Numbers: ");
			foreach(var elements in arr)
			{
				if(elements%2 == 1)
				{
					Console.Write(elements + " ");
				}
			}
			Console.WriteLine();
			Console.WriteLine("Even Numbers: ");
			foreach(var elements in arr)
			{
				if(elements%2 == 0)
				{
					Console.Write(elements + " ");
				}
			}
				
			Console.ReadKey(true);
		}
	}
}